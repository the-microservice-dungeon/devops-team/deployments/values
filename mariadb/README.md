
---

kubectl create ns databases
kubectl -n databases apply -f configmapdb.yaml

helm -n databases install mariadb oci://registry-1.docker.io/bitnamicharts/mariadb -f values.yaml --create-namespace --dry-run

kubectl -n databases create secret generic mariadb-secret \
--from-literal=mariadb-root-password='root' \
--from-literal=mariadb-replication-password='replicationpassword' \
--from-literal=mariadb-password='admin'

---

**to access databases external**

kubectl -n databases port-forward svc/mariadb 3306:3306

![img.png](mariadb.png)

---

the ./mariadb.sh is for testing purpose

---

# TODOS
* Create secrets for users
* implement this as extra env vars or value mount
* initdb script cant access environment so the initdb must be a sh
* name the sh setup.sh
* setup.sh should be inside a configmap
