#!/usr/bin/bash

# Only for testing

kubectl create ns databases
kubectl -n databases apply -f configmapdb.yaml


kubectl -n databases create secret generic mariadb-secret \
--from-literal=mariadb-root-password='root' \
--from-literal=mariadb-replication-password='replicationpassword' \
--from-literal=mariadb-password='admin'
 
kubectl -n databases create secret generic mariadb-user-secret \
--from-literal=game-password='game' \
--from-literal=gamelog-password='gamelog' \
--from-literal=robot-password='robot' \
--from-literal=trading-password='trading'

# helm -n databases install mariadb oci://registry-1.docker.io/bitnamicharts/mariadb -f values.yaml --create-namespace --dry-run

helm -n databases install mariadb oci://registry-1.docker.io/bitnamicharts/mariadb -f values.yaml --create-namespace

kubectl -n databases rollout status statefulset

