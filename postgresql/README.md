
---

kubectl create ns databases
kubectl -n databases apply -f configmapdb.yaml

helm -n databases install postgresql oci://registry-1.docker.io/bitnamicharts/postgresql -f values.yaml --create-namespace --dry-run

kubectl -n databases create secret generic postgresql-secret \
--from-literal=postgres-password='root' \
--from-literal=replication-password='replicationpassword' \
--from-literal=password='admin'

---

**to access databases external**

kubectl -n databases port-forward svc/postgresql 3306:3306

![img.png](postgresql.png)

---

the ./postgresql.sh is for testing purpose

---

# TODOS
* Create secrets for users
* implement this as extra env vars or value mount
* initdb script cant access environment so the initdb must be a sh
* name the sh setup.sh
* setup.sh should be inside a configmap
