#!/usr/bin/bash

# Only for testing

kubectl create ns databases
kubectl -n databases apply -f configmapdb.yaml


kubectl -n databases create secret generic postgresql-secret \
--from-literal=postgres-password='root' \
--from-literal=replication-password='replicationpassword' \
--from-literal=password='admin' \

kubectl -n databases create secret generic postgresql-user-secret \
--from-literal=map-password='map'


# helm -n databases install postgresql oci://registry-1.docker.io/bitnamicharts/postgresql -f values.yaml --create-namespace --dry-run

helm -n databases install postgresql oci://registry-1.docker.io/bitnamicharts/postgresql -f values.yaml --create-namespace

kubectl -n databases rollout status statefulset

